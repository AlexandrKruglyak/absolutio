<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m190514_094235_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'keywords' => $this->string(255),
            'project_name' => $this->string(255),
            'project_description' => $this->text(),
            'project_img' => $this->string(255),
            'section_1_title' => $this->string(255),
            'section_1_text' => $this->text(),
            'section_2_title' => $this->string(255),
            'section_2_text' => $this->text(),
            'client_name' => $this->string(255),
            'website' => $this->string(255),
            'services' => $this->text(),
            'block_1_title' => $this->string(255),
            'block_1_text' => $this->text(),
            'block_1_img' => $this->string(255),
            'block_2_title' => $this->string(255),
            'block_2_text' => $this->text(),
            'block_2_img' => $this->string(255),
            'block_3_title' => $this->string(255),
            'block_3_text' => $this->text(),
            'block_3_img' => $this->string(255),
            'block_4_title' => $this->string(255),
            'block_4_text' => $this->text(),
            'block_4_img_1' => $this->string(255),
            'block_4_img_2' => $this->string(255),
            'block_4_img_3' => $this->string(255),
            'review_name' => $this->string(255),
            'review_position' => $this->string(255),
            'review_text' => $this->text(),
            'review_img' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%projects}}');
    }
}

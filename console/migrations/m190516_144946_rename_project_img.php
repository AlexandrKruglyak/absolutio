<?php

use yii\db\Migration;

/**
 * Class m190516_144946_rename_project_img
 */
class m190516_144946_rename_project_img extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('projects','project_img','project_img_1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190516_144946_rename_project_img cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190516_144946_rename_project_img cannot be reverted.\n";

        return false;
    }
    */
}

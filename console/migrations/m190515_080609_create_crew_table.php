<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crew}}`.
 */
class m190515_080609_create_crew_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crew}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'position' => $this->string(255),
            'img' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crew}}');
    }
}

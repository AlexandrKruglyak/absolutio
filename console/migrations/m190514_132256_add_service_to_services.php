<?php

use yii\db\Migration;

/**
 * Class m190514_132256_add_service_to_services
 */
class m190514_132256_add_service_to_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('services' ,'block_5_title', $this->string(255));
        $this->addColumn('services' ,'block_5_text', $this->text());
        $this->addColumn('services' ,'block_5_img', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190514_132256_add_service_to_services cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190514_132256_add_service_to_services cannot be reverted.\n";

        return false;
    }
    */
}

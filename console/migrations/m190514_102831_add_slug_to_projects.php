<?php

use yii\db\Migration;

/**
 * Class m190514_102831_add_slug_to_projects
 */
class m190514_102831_add_slug_to_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'slug', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190514_102831_add_slug_to_projects cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190514_102831_add_slug_to_projects cannot be reverted.\n";

        return false;
    }
    */
}

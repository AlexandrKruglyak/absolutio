<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%main_page}}`.
 */
class m190513_172602_create_main_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%main_page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'keywords' => $this->string(255),
            'section_1_title' => $this->string(255),
            'section_1_text' => $this->text(),
            'section_1_link' => $this->string(255),
            'section_1_img' => $this->string(255),
            'section_2_title' => $this->string(255),
            'section_2_text' => $this->text(),
            'section_2_img' => $this->string(255),
            'services_title' => $this->string(255),
            'services_text' => $this->text(),
            'services_1' => $this->integer(11),
            'services_2' => $this->integer(11),
            'services_3' => $this->integer(11),
            'services_4' => $this->integer(11),
            'work_title' => $this->string(255),
            'work_text' => $this->text(),
            'crew_title' => $this->string(255),
            'crew_text' => $this->text(),
            'footer_left_title' => $this->string(255),
            'footer_left_text' => $this->text(),
            'footer_right_title' => $this->string(255),
            'footer_right_text' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%main_page}}');
    }
}

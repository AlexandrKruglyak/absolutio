<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact}}`.
 */
class m190515_085031_create_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'keywords' => $this->string(255),
            'contact_title' => $this->string(255),
            'contact_text' => $this->text(),
            'contact_phone' => $this->string(255),
            'contact_email' => $this->string(255),
            'contact_address' => $this->string(255),
            'facebook' => $this->string(255),
            'instagram' => $this->string(255),
            'twitter' => $this->string(255),
            'linkedin' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contact}}');
    }
}

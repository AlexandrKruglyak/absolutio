<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%services}}`.
 */
class m190514_130508_create_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%services}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'keywords' => $this->string(255),
            'service_name' => $this->string(255),
            'service_description' => $this->text(),
            'block_1_title' => $this->string(255),
            'block_1_text' => $this->text(),
            'block_1_img' => $this->string(255),
            'block_2_title' => $this->string(255),
            'block_2_text' => $this->text(),
            'block_2_img' => $this->string(255),
            'block_3_title' => $this->string(255),
            'block_3_text' => $this->text(),
            'block_3_img' => $this->string(255),
            'block_4_title' => $this->string(255),
            'block_4_text' => $this->text(),
            'block_4_img' => $this->string(255),
            'slug' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%services}}');
    }
}

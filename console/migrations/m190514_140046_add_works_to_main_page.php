<?php

use yii\db\Migration;

/**
 * Class m190514_140046_add_works_to_main_page
 */
class m190514_140046_add_works_to_main_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('main_page','work_1', $this->integer(11));
        $this->addColumn('main_page','work_2', $this->integer(11));
        $this->addColumn('main_page','work_3', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190514_140046_add_works_to_main_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190514_140046_add_works_to_main_page cannot be reverted.\n";

        return false;
    }
    */
}

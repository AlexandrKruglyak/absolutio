<?php

namespace frontend\models;

use common\models\Notification;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $message;
//    public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            [['phone'],'string'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $model = new Notification();
        $model->name = $this->name;
        $model->email = $this->email;
        $model->phone = $this->phone;
        $model->message = $this->message;
        $model->save();
        return  Yii::$app->mailer->compose('contact')
        ->setTo($email)
        ->setFrom('informer@absolut.io')
        ->setSubject('New notification in Absolut.io')
        ->send();
    }
}

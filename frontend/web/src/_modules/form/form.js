import swal from 'sweetalert2';
import FormValidator from 'validate-js';

export default class Form {
    constructor() {
        this.errMessages = [
            { confirmPass: 'The password confirmation field is required' },
            { currency: 'The currency field is required' },
            { email: 'The email field is required' },
            { endDate: 'The end date field is required' },
            { fullname: 'The fullname field is required' },
            { phone: 'The phone field is required' },
            { password: 'The password field is required' },
            { startDate: 'The start date field is required' },
            { validConfirmPassword: 'Password has been entered incorrectly' },
            { validDate: 'Date must be mm.dd.yy format' },
            { validEmail: 'The email field must contain a valid email address' },
            { validPassword: 'Password must contain at least 1 lowercase alphabetical character, at least 1 uppercase alphabetical character, at least 1 lowercase numeric character, at least 1 special character' },
        ];
        this.forms = Array.from(document.querySelectorAll('.validate'));
    }

    init() {
        this.forms.forEach(form => {
            const formID = form.id;
            const formElements = Array.from(form.elements).filter((el) => el.type !== 'hidden' && el.type !== 'file' && el.type !== 'button');

            let validator = new FormValidator( formID,
                [
                    {
                        name: 'agreement',
                        rules: 'required'
                    },
                    {
                        name: 'campaign',
                        rules: 'required'
                    },
                    {
                        name: 'company',
                        rules: 'required'
                    },
                    {
                        name: 'campaignName',
                        display: 'campaign name',
                        rules: 'required'
                    },
                    // TODO: Uncomment below
                    // {
                    //     name: 'confirmPass',
                    //     display: 'password confirmation',
                    //     rules: 'required|matches[password]'
                    // },
                    {
                        name: 'currency',
                        rules: 'required'
                    },
                    {
                        name: 'email',
                        rules: 'required|valid_email'
                    },
                    {
                        name: 'endDate',
                        display: 'end date',
                        rules: 'required|callback_date'
                    },
                    {
                        name: 'fullname',
                        display: 'fullname',
                        rules: 'required'
                    },
                    // TODO: Uncomment below
                    // {
                    //     name: 'password',
                    //     rules: 'required|min_length[8]|callback_strong_password'
                    // },
                    {
                        name: 'role',
                        rules: 'required'
                    },
                    {
                        name: 'startDate',
                        display: 'start date',
                        rules: 'required|callback_date'
                    },
                    {
                        name: 'phone',
                        rules: 'required'
                    },
                    {
                        name: 'user',
                        rules: 'required'
                    }
                ],
                function(errors, event) {
                    const form = event.target.closest('form');

                    form.dataset.errors = '0';

                    const validBlocks = form.querySelectorAll('.form-field__valid');
                    const errorBlocks = form.querySelectorAll('.form-field__error');

                    validBlocks.forEach((item) => {
                        let el = item.closest('.has-valid');
                        if (el) {
                            el.classList.remove('has-valid');
                        }
                        item.remove();
                    });
                    errorBlocks.forEach((item) => {
                        let el = item.closest('.has-error');
                        if (el) {
                            el.classList.remove('has-error');
                        }
                        item.remove();
                    });

                    if (errors.length > 0) {
                        // Show the errors
                        console.log('Errors', errors);
                        form.dataset.errors = JSON.stringify(errors);

                        errors.forEach((error) => {
                            const el = error.element;
                            const parent = el.closest('.form-field--error-wrap');
                            parent.classList.add('has-error');
                            let errorText = document.createElement('div');
                            errorText.classList.add('form-field__error');
                            errorText.textContent = error.message;
                            parent.appendChild(errorText);
                        });
                    }
                }
            );

            validator.registerCallback('strong_password', function(value) {
                let regexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
                // lcAlpha = /(?=.*[a-z])/,
                // ucAlpha = /(?=.*[A-Z])/,
                // num = /(?=.*[0-9])/,
                // specialChar = /(?=.*[!@#\$%\^&\*])/;
                if (!regexp.test(value)) {
                    return false;
                }

                return true;
            })
            // at least 8 characters, at least one special character, at least one capital letter, both letter and number.
            .setMessage('strong_password', 'Password must contain at least 1 lowercase alphabetical character, at least 1 uppercase alphabetical character, at least 1 lowercase numeric character, at least 1 special character.');

            validator.registerCallback('date', function(value) {
                let regexp = /^(((0)[0-9])|((1)[0-2]))(.)([0-2][0-9]|(3)[0-1])(.)\d{4}$/;
                if (!regexp.test(value)) {
                    return false;
                }

                return true;
            })
            .setMessage('date', 'Date must be mm.dd.yy format');

            validator.registerCallback('url_with_port', function(value) {
                // let regexp = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|):([0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                let regexp = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|):(\d{2,4})/;
                if (!regexp.test(value)) {
                    return false;
                }

                return true;
            })
            .setMessage('url_with_port', 'The url must be like https://yoursite:PORT');

            formElements.forEach((el) => {
                el.addEventListener('change', this.onChangeFormElement.bind(this));
                if (el.classList.contains('selectric-sel')) {
                    initSelectric('.selectric-sel').on('selectric-change', this.onChangeFormElement.bind(this));
                }
            });

            form.addEventListener('submit', function (event) {
                event.preventDefault();

                const data = $(this).serialize();

                if (validator.errors.length === 0) {
                    switch (formID) {
                        case "signup":
                            signup(data, formID);
                            break;
                        default:
                    }
                }
            });
        });

    }

    onChangeFormElement(event) {
        let target = event.target,
            parent = target.closest('.form-field--error-wrap');

        let validEl = document.createElement('div'),
            span = document.createElement('span');

        validEl.appendChild(span);
        validEl.classList.add('form-field__valid');

        let errorEl = document.createElement('div');
        errorEl.classList.add('form-field__error');
        const errMsg = this.errMessages.filter((msg) => msg[target.name]);
        if (!errMsg.length) {
            return;
        }
        errorEl.textContent = errMsg[0][target.name];

        deleteErrors();
        deleteValids();

        if (!target.value) {
            if (target.name === 'startDate' ||
            target.name === 'endDate')
            {
                if (!parentHalf.classList.contains('has-error')) {
                    parentHalf.classList.add('has-error');
                    parentHalf.appendChild(errorEl);
                }
            } else {
                // TODO: refactor it
                if (target.name === 'companyName' || target.name === 'companies') {
                    return;
                }

                if (!parent.classList.contains('has-error')) {
                    parent.classList.add('has-error');
                    parent.appendChild(errorEl);
                }
            }
        } else {
            if (target.name === 'email') {
                const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
                if (re.test(target.value)) {

                    if (!parent.classList.contains('has-valid')) {
                        parent.querySelector('.form-field__wrapper').appendChild(validEl);

                        setTimeout(() => {
                            parent.classList.add('has-valid');
                        }, 50);
                    }

                } else {

                    if (!parent.classList.contains('has-error')) {
                        parent.classList.add('has-error');
                        const errMsg = this.errMessages.filter((msg) => msg['validEmail']);
                        errorEl.textContent = errMsg[0]['validEmail'];
                        parent.appendChild(errorEl);
                    }
                }
            } else if (target.name === 'password') {
                let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
                // TODO: Uncomment the line below
                // if (re.test(target.value)) {
                    if (target.value) {
                        // deleteErrors();

                        if (!parent.classList.contains('has-valid')) {
                            parent.querySelector('.form-field__wrapper').appendChild(validEl);

                            setTimeout(() => {
                                parent.classList.add('has-valid');
                            }, 50);
                        }
                    } else {
                        // deleteValids();

                        if (!parent.classList.contains('has-error')) {
                            parent.classList.add('has-error');
                            const errMsg = this.errMessages.filter((msg) => msg['validPassword']);
                            errorEl.textContent = errMsg[0]['validPassword'];
                            parent.appendChild(errorEl);
                        }
                    }
                } else if (target.name === 'confirmPass') {
                    let pass = document.getElementById('password');

                    if (pass.value === '') {
                        deleteValids();
                        return;
                    }
                    if (pass.value === target.value) {

                        deleteErrors();

                        if (!parent.classList.contains('has-valid')) {
                            parent.querySelector('.form-field__wrapper').appendChild(validEl);

                            setTimeout(() => {
                                parent.classList.add('has-valid');
                            }, 50);
                        }
                    } else {
                        // deleteValids();

                        if (!parent.classList.contains('has-error')) {
                            parent.classList.add('has-error');
                            const errMsg = this.errMessages.filter((msg) => msg['validConfirmPassword']);
                            errorEl.textContent = errMsg[0]['validConfirmPassword'];
                            parent.appendChild(errorEl);
                        }
                    }
                } else if (target.name === 'startDate' || target.name === 'endDate') {
                    const re = /^(((0)[0-9])|((1)[0-2]))(.)([0-2][0-9]|(3)[0-1])(.)\d{4}$/; // mm.dd.yy format
                    if (!re.test(target.value)) {

                        if (!parentHalf.classList.contains('has-error')) {
                            parentHalf.classList.add('has-error');
                            const errMsg = this.errMessages.filter((msg) => msg['validDate']);
                            errorEl.textContent = errMsg[0]['validDate'];
                            parentHalf.appendChild(errorEl);
                        }
                    }
                } else if (target.name === 'leadsApiUrl') {
                    const re = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|):(\d{2,4})/;
                    if (!re.test(target.value)) {

                        if (!parent.classList.contains('has-error')) {
                            parent.classList.add('has-error');
                            const errMsg = this.errMessages.filter((msg) => msg['url_with_port']);
                            errorEl.textContent = errMsg[0]['url_with_port'];
                            parent.appendChild(errorEl);
                        }
                    }
                }
            }

        function deleteErrors() {
            // const parentEl = parentElement ? parentElement : parent;
            let errorBlocks = parentEl.querySelectorAll('.form-field__error');
            errorBlocks.forEach((item) => {
                let e = item.closest('.has-error');
                if (e) {
                    e.classList.remove('has-error');
                }
                item.remove();
            });
            parentEl.classList.remove('has-error');
        }
        function deleteValids() {
                let validBlocks = parent.querySelectorAll('.form-field__valid');
                validBlocks.forEach((item) => {
                    let v = item.closest('.has-valid');
                    if (v) {
                        v.classList.remove('has-valid');
                    }
                    item.remove();
                });
                parent.classList.remove('has-valid');
            }
    }

    signup(data, formID) {
        const form = document.getElementById(formID);
        const submitBtn = form.querySelector('button[type="submit"]');

        submitBtn.disabled = true;

        $.ajax({
            url: '/signup',
            type: 'post',
            data: data,
            success: function (data) {
                window.location='/signin';
            },
            error: function (xhr) {
                errorCb(xhr, submitBtn);
            }
        });
    }

    errorCb(xhr, btn) {
        const error = JSON.parse(xhr.responseText);

        swal({
            title: 'Oops..',
            text: error.message,
            type: 'error',
            showCloseButton: true,
            showConfirmButton: true,
            showCancelButton: false,
        });

        if (btn) {
            btn.disabled = false;
        }
    }

}

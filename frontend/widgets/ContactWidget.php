<?php
namespace frontend\widgets;

use yii\base\Widget;
use common\models\Contact;

class ContactWidget extends Widget
{

    public function run()
    {
        $model = Contact::findOne(Contact::findOne(Contact::ID));
        return $this->render('contact',['model' => $model]);
    }
}
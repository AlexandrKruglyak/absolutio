<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\models\MainPage;

AppAsset::register($this);
$model = MainPage::findOne(MainPage::ID);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!--[if lt IE 11]>
<p class='browsehappy'>You are using an <strong>outdated</strong> browser. Please <a href='http://browsehappy.com/'>upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<?php if($_SERVER['REQUEST_URI'] === '/'): ?>
<div class="shapes shapes--home">
    <div class="shapes__digital">
        <img src="<?= ($model->section_1_img) ? $model->section_1_img : '../images/pic-digital.png' ?>" alt="" class="image-custom__stroke image-custom__stroke--s2">
    </div>
</div>
<?php endif;?>

<header class="page-header">
    <div class="grid-container">
        <div class="top-logo"><a href="/"><img src="../images/logo.svg" alt=""></a></div>
        <nav>
            <ul class="main-menu">
                <li><a href="/">About</a></li>
                <li><a href="<?= ($_SERVER['REQUEST_URI'] == '/') ? '#services' : '/#services'?>">Services</a></li>
                <li><a href="/work">Our Work</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="<?= ($_SERVER['REQUEST_URI'] == '/') ? '#crew' : '/#crew'?>">Crew</a></li>
            </ul>
            <span class="hide-for-large main-menu__dropdown"></span>
        </nav>
    </div>
</header>
<main class="main-content">
    <?= $content ?>
</main>

<footer class="page-footer">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell medium-4 large-4">
                <div class="footer-logo">
                    <a href="/">
                        <img src="../images/logo-white.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="cell medium-4 large-4">
                <div class="page-footer__title"><?= $model->footer_left_title ?></div>
                <p class="text">
                    <?= $model->footer_left_text ?>
                </p>
            </div>
            <div class="cell medium-4 large-4">
                <div class="page-footer__title"><?= $model->footer_right_title ?></div>
                <p class="text">
                    <?= $model->footer_right_text ?>
                </p>
            </div>
        </div>
    </div>
</footer>
<svg width="0" height="0" style="max-height:0;">
    <defs>
        <clipPath id="shape1">
            <path d="M56.1,60.6C-48.4,144,13.2,222,78.6,275c65.4,53,161.2,80.7,214.5-22.5C346.3,149.3,345.7,85.8,270.5,38  c-36.2-23-63.1-38-92.9-38C145.6,0,110.2,17.3,56.1,60.6"></path>
        </clipPath>
        <clipPath id="shape2">
            <path d="M182.3,3.6C13.8,36,3.5,118.5,0.5,155.8c-5,63.5,23.9,186,160.8,186h0.1c11,0,22.8-0.8,35.3-2.5  c167.4-22.7,167.4-114.9,167.4-144.3c0-27.2-11.4-195-145.8-195.1C207.2,0,195.2,1.1,182.3,3.6"></path>
        </clipPath>
        <clipPath id="shape3">
            <path d="M71.4,299.6c105.5,54.4,170.3,53.7,219.2-23c48.9-76.8,62.3-112.5-23-219.2c-85.3-106.8-165-43.9-219.2,23  C-5.8,147.3-34.1,245.2,71.4,299.6"></path>
        </clipPath>
        <clipPath id="shape4-mobile">
            <path d="M39.55 165.92C98 196 133.88 195.66 161 153.18s34.51-62.31-12.74-121.41S56.83 7.45 26.81 44.51s-45.7 91.28 12.74 121.41"></path>
        </clipPath>
        <clipPath id="shape4">
            <path d="M49.6 53.58c-92.39 73.74-37.93 142.7 19.89 189.56S212 314.49 259.15 223.25s46.51-147.39-20-189.65C207.16 13.26 183.38 0 157 0 128.73 0 97.44 15.3 49.6 53.58"></path>
        </clipPath>
        <clipPath id="shape5-mobile">
            <path d="M99.72 2C7.56 19.74 1.93 64.87.29 85.27-2.45 120 13.36 187 88.24 187h.05a143.61 143.61 0 0 0 19.31-1.37c91.56-12.42 91.56-62.84 91.56-78.92C199.16 91.83 192.92.05 119.41 0a108.4 108.4 0 0 0-19.69 2"></path>
        </clipPath>
        <clipPath id="shape5">
            <path d="M152 3.08C11.53 30.09 2.94 98.86.44 130c-4.17 52.93 19.92 155 134 155h.08a218.87 218.87 0 0 0 29.48-2.08C303.53 264 303.53 187.14 303.53 162.63 303.53 140 294 .08 182 0a165.21 165.21 0 0 0-30 3.08"></path>
        </clipPath>
        <clipPath id="shape6-mobile">
            <path d="M32.55 35.16C-28.08 83.54 7.66 128.79 45.6 159.54s93.52 46.81 124.4-13.06S200.55 49.78 156.93 22C135.93 8.7 120.32 0 103 0 84.47 0 63.93 10 32.55 35.16"></path>
        </clipPath>
        <clipPath id="shape6">
            <path d="M60.28 252.87c89.06 45.92 143.76 45.33 185-19.42s52.59-95-19.42-185-139.25-37.09-185 19.38-69.64 139.12 19.42 185"></path>
        </clipPath>
    </defs>
</svg>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135322497-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135322497-2');
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $contact \common\models\Contact */

$this->title = $contact->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $contact->description
]);
$this->registerMetaTag([
    'name' => 'Keywords',
    'content' => $contact->keywords
]);
?>

<div class="grid-container page-contact">
    <div class="shapes">
        <div class="shapes__item-1 shapes__item-1--p2"></div>
    </div>
    <div class="grid-x grid-padding-x align-justify">
        <div class="cell medium-12 large-5">
            <div class="contact-box__title"><?= $contact->contact_title?></div>
            <div class="contact-box__text"><?= $contact->contact_text?></div>
            <div class="contact-box__item phone"><a href="tel:<?= str_replace([' ','-'],'',$contact->contact_phone)?>"><?= $contact->contact_phone?></a></div>
            <div class="contact-box__item email"><a href="mailto:<?= $contact->contact_email?>"><?= $contact->contact_email?></a></div>
            <div class="contact-box__item map">
                <address>
                    <?= $contact->contact_address?>
                </address>
            </div>
            <ul class="contact-box__social">
                <?php
                echo ($contact->facebook) ? Html::tag('li', Html::a('',$contact->facebook,['class' => 'icon-facebook', 'target' => '_blank'])) : '';
                echo ($contact->instagram) ? Html::tag('li', Html::a('',$contact->instagram,['class' => 'icon-instagram', 'target' => '_blank'])) : '';
                echo ($contact->twitter) ? Html::tag('li', Html::a('',$contact->twitter,['class' => 'icon-twitter', 'target' => '_blank'])) : '';
                echo ($contact->linkedin) ? Html::tag('li', Html::a('',$contact->linkedin,['class' => 'icon-linkedin', 'target' => '_blank'])) : '';
                ?>
            </ul>
        </div>
        <div class="cell medium-12 large-6">
            <div class="contact-box__form">
                <?= \backend\widgets\Alert::widget()?>
                <div class="title">Drop us a line</div>
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'fieldConfig' => [

                            'options' => ['class' => 'field']
                    ]
                ]); ?>

                <?= $form->field($model, 'name')->textInput() ?>

                <?= $form->field($model, 'email')->textInput() ?>

                <?= $form->field($model, 'phone')->textInput() ?>

                <?= $form->field($model, 'message')->textarea() ?>

                <div class="submit">
                    <button type="submit" class="button">Submit</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Services */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description
]);
$this->registerMetaTag([
    'name' => 'Keywords',
    'content' => $model->keywords
]);
?>
<div class="grid-container">
    <h2 class="main-title"><?= $model->service_name ?></h2>
    <p class="page-home__title-text">
        <?= Html::encode($model->service_description) ?>
    </p>
    <div class="project-panel project-panel--seo">
        <div class="shapes">
            <div class="shapes__item-1"></div>
        </div>
        <?php if($model->block_1_title && $model->block_1_text):?>
        <div class="grid-x grid-padding-x align-justify">
            <div class="cell medium-6 large-6">
                <div class="middle-title middle-title--s1"><?= $model->block_1_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->block_1_text) ?>
                </div>
            </div>
            <div class="cell medium-5 large-5">
                <div class="project-panel__image">
                    <div class="clip-image clip-image--s1"><img src="<?= ($model->block_1_img) ? $model->block_1_img : '../images/clip-test/pic.jpg' ?>" alt="" class="clip-image__shape-1"></div>
                </div>
            </div>
        </div>
        <?php endif;?>

        <?php if($model->block_2_title && $model->block_2_text):?>
        <div class="grid-x grid-padding-x align-justify">
            <div class="cell medium-4 large-4 project-panel__col-order">
                <div class="project-panel__image project-panel__image-----p1">
                    <div class="clip-image clip-image--s2"><img src="<?= ($model->block_2_img) ? $model->block_2_img : '../images/clip-test/pic1.jpg' ?>" alt="" class="clip-image__shape-2"></div>
                </div>
            </div>
            <div class="cell medium-6 large-6">
                <div class="shapes">
                    <div class="shapes__item-2 shapes__item-2--p1"></div>
                </div>
                <div class="middle-title middle-title--s1"><?= $model->block_2_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->block_2_text) ?>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
</div>
<!-- <div class="shapes">
    <div class="shapes__item-4"></div>
</div> -->
<div class="grid-container page-services">
    <div class="project-panel project-panel--seo">
        <?php if($model->block_3_title && $model->block_3_text):?>
        <div class="grid-x grid-padding-x align-justify project-panel__row">
            <div class="cell medium-6 large-6">
                <div class="middle-title middle-title--s1"><?= $model->block_3_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->block_3_text) ?>
                </div>
            </div>
            <div class="cell medium-5 large-5">
                <div class="project-panel__image">
                    <div class="clip-image clip-image--s3"><img src="<?= ($model->block_3_img) ? $model->block_3_img : '../images/clip-test/pic2.jpg' ?>" alt="" class="clip-image__shape-3"></div>
                </div>
            </div>
        </div>
        <?php endif;?>

        <?php if($model->block_4_title && $model->block_4_text):?>
        <div class="grid-x grid-padding-x align-justify project-panel__row1">
            <div class="cell medium-4 large-4 project-panel__col-order">
                <div class="project-panel__image project-panel__image-----p1">
                    <div class="clip-image clip-image--s2"><img src="<?= ($model->block_4_img) ? $model->block_4_img : '../images/clip-test/pic3.jpg' ?>" alt="" class="clip-image__shape-2"></div>
                </div>
            </div>
            <div class="cell medium-6 large-6">
                <div class="shapes">
                    <div class="shapes__item-7 shapes__item-7--p1"></div>
                </div>
                <div class="middle-title middle-title--s1"><?= $model->block_4_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->block_4_text) ?>
                </div>
            </div>
        </div>
        <?php endif;?>

        <?php if($model->block_5_title && $model->block_5_text):?>
        <div class="grid-x grid-padding-x align-justify project-panel__row2">
            <div class="cell medium-6 large-6">
                <div class="shapes">
                    <div class="shapes__item-1 shapes__item-1--p3"></div>
                </div>
                <div class="middle-title middle-title--s1"><?= $model->block_5_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->block_5_text) ?>
                </div>
            </div>
            <div class="cell medium-5 large-5">
                <div class="project-panel__image">
                    <div class="clip-image clip-image--s1"><img src="<?= ($model->block_5_img) ? $model->block_5_img : '../images/clip-test/pic4.jpg' ?>" alt="" class="clip-image__shape-1"></div>
                </div>
            </div>
        </div>
        <?php endif;?>

    </div>

    <?= \frontend\widgets\ContactWidget::widget() ?>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ListView;

?>


<div class="grid-container">
    <h2 class="main-title"><?= ($work_page->title) ? $work_page->title : '' ?></h2>
    <p class="page-home__title-text">
        <?= Html::encode(($work_page->description) ? $work_page->description : '') ?>
    </p>

    <div class="project-panel project-panel--seo">
        <div class="shapes">
            <div class="shapes__item-1"></div>
        </div>

        <?=  ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list_work',
            'pager' => [
                'options' => [
                    'class' => 'pagination pagination--m-60'
                ],
            ],
            'summary' => ''
        ]);?>
    </div>
</div>

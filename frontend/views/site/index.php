<?php

/* @var $this yii\web\View */
/* @var $model \common\models\MainPage */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description
]);
$this->registerMetaTag([
    'name' => 'Keywords',
    'content' => $model->keywords
]);
?>
<div class="grid-container">
    <div class="grid-x grid-padding-x align-justify">
        <div class="cell medium-6 large-5">
            <div class="shapes">
                <div class="shapes__item-1"></div>
            </div>
            <div class="digital-box">
                <div class="digital-box__title"><?= $model->section_1_title ?></div>
                <p class="digital-box__text"><?= $model->section_1_text ?></p>
                <div class="digital-box__more"><a href="#item-2" class="button">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="innovation-box">
    <div class="innovation-box__image"><img src="<?= ($model->section_2_img) ? $model->section_2_img : '../images/pic-innovation.png' ?>" alt=""
                                            class="image-custom__stroke image-custom__stroke--s3"></div>
    <div class="innovation-box__wrap">
        <div class="grid-container">
            <div class="shapes">
                <div class="shapes__item-2"></div>
            </div>
            <div id="item-2" class="grid-x grid-padding-x custom-row">
                <div class="cell medium-7 medium-offset-5 large-5 large-offset-6 custom-cell">
                    <div class="shapes">
                        <div class="shapes__item-3"></div>
                    </div>
                    <div class="innovation-box__title"><?= $model->section_2_title ?></div>
                    <p class="innovation-box__text">
                        <?= $model->section_2_text ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="services" class="grid-container">
    <h2 class="main-title main-title--s3"><?= $model->services_title ?></h2>
    <p class="page-home__title-text">
        <?= $model->services_text ?>
    </p>
</div>
<div class="services-menu">
    <a href="/s-<?= $model->services1->slug?>" class="menu-1"><?= $model->services1->service_name?></a>
    <a href="/s-<?= $model->services2->slug?>" class="menu-2"><?= $model->services2->service_name?></a>
    <a href="/s-<?= $model->services3->slug?>" class="menu-3"><?= $model->services3->service_name?></a>
    <a href="/s-<?= $model->services4->slug?>" class="menu-4"><?= $model->services4->service_name?></a>
</div>
<div class="shapes">
    <div class="shapes__item-4"></div>
    <div class="shapes__item-5"></div>
</div>
<div id="work" class="grid-container">
    <h2 class="main-title main-title--s3"><?= $model->work_title ?></h2>
    <p class="page-home__title-text">
        <?= $model->work_text ?>
    </p>
    <div class="project-panel project-panel--seo">
       <?= $this->render('__main_projects',['model' => $model]) ?>
    </div>
</div>
<div class="shapes">
    <div class="shapes__item-8"></div>
    <div class="shapes__item-9"></div>
</div>

<div id="crew" class="grid-container page-home">
    <h2 class="main-title main-title--s2"> <?= $model->crew_title ?></h2>
    <p class="page-home__title-text"><?= $model->crew_text ?></p>
    <?php if($crew):?>
    <div class="slider-team">
        <?php $i=4 ?>
        <?php foreach ($crew as $item): ?>
        <?php $i=($i>6) ? 4 : $i ?>
        <div class="slider-team__slide">
            <div class="slider-team__image">
                <img src="<?= ($item->img) ? $item->img : '../images/pic-slide.png' ?>" class="slider-team__image-shape<?= $i ?>" alt="">
            </div>
            <div class="slider-team__name"><?= $item->name?></div>
            <div class="slider-team__position"><?= $item->position?></div>
        </div>
        <?php $i++ ?>
        <?php endforeach;?>
    </div>
    <?php endif;?>
</div>

<?php
use yii\helpers\StringHelper;
/* @var $model \common\models\MainPage */
?>
<?php if ($model->work_1): ?>
    <div class="grid-x grid-padding-x align-justify">
        <div class="cell medium-5 large-5">
            <div class="middle-title middle-title--s1"><?= $model->work1->project_name ?></div>
            <div class="project-panel__text">
                <?= StringHelper::truncate($model->work1->project_description,190) ?>
            </div>
            <div class="project-panel__more">
                <a href="/p-<?= $model->work1->slug ?>" class="button small">Read more</a>
            </div>
        </div>
        <div class="cell medium-5 large-5">
            <div class="project-panel__image">
                <div class="clip-image clip-image--s1">
                    <img src="<?= ($model->work1->project_img_1) ? $model->work1->project_img_1 : '../images/clip-test/pic1.jpg' ?>" alt="" class="clip-image__shape-1">
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($model->work_2): ?>
    <div class="grid-x grid-padding-x align-justify project-panel__row">
        <div class="cell medium-4 large-4 project-panel__col-order">
            <div class="project-panel__image project-panel__image-----p1">
                <div class="clip-image clip-image--s2">
                    <img src="<?= ($model->work2->project_img_1) ? $model->work2->project_img_1 : '../images/clip-test/pic1.jpg' ?>" alt="" class="clip-image__shape-2"></div>
            </div>
        </div>
        <div class="cell medium-5 large-5">
            <div class="middle-title middle-title--s1"><?= $model->work2->project_name ?></div>
            <div class="project-panel__text">
                <?=  StringHelper::truncate($model->work2->project_description,190) ?>
            </div>
            <div class="project-panel__more"><a href="/p-<?= $model->work2->slug ?>" class="button small">Read more</a>
            </div>
        </div>
    </div>
    <div class="shapes">
        <div class="shapes__item-6"></div>
    </div>
<?php endif; ?>
<?php if ($model->work_3): ?>
    <div class="grid-x grid-padding-x align-right">
        <div class="cell medium-5 large-5">
            <div class="shapes">
                <div class="shapes__item-7"></div>
            </div>
            <div class="middle-title middle-title--s1"><?= $model->work3->project_name ?></div>
            <div class="project-panel__text">
                <?=  StringHelper::truncate($model->work3->project_description,190) ?>
            </div>
            <div class="project-panel__more">
                <a href="/p-<?= $model->work3->slug ?>" class="button small">Read more</a>
            </div>
        </div>
        <div class="cell medium-4 large-4">
            <div class="project-panel__image project-panel__image--p2">
                <div class="clip-image clip-image--s3">
                    <img src="<?= ($model->work3->project_img_1) ? $model->work3->project_img_1 : '../images/clip-test/pic1.jpg' ?>" alt="" class="clip-image__shape-3">
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
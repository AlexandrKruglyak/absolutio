<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Projects */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description
]);
$this->registerMetaTag([
    'name' => 'Keywords',
    'content' => $model->keywords
]);
?>

<div class="grid-container page-projects">
    <h2 class="main-title"><?= $model->project_name?></h2>
    <p class="page-home__title-text">
        <?= Html::encode($model->project_description) ?>
    </p>
    <div class="project-panel">
        <div class="shapes">
            <div class="shapes__item-1 shapes__item-1--p1"></div>
        </div>
        <div class="grid-x grid-padding-x align-justify">
            <div class="cell medium-6 large-6">
                <div class="middle-title"><?= $model->section_1_title?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->section_1_text) ?>
                </div>
            </div>
            <div class="cell medium-4 large-4">
                <ul class="project-panel__nav">
                    <li class="title">Client</li>
                    <li><?= $model->client_name ?></li>
                    <li class="title">Services provided</li>
                    <?= $model->renderServices() ?>
                    <li class="title">Take a look</li>
                    <li>
                        <?= Html::a('View the website', $model->website, ['class' => 'site', 'target' => '_blank']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-justify">
            <div class="cell medium-4 large-4">
                <div class="project-panel__image project-panel__image-----p1">
                    <div class="clip-image clip-image--s2"><img src="<?= ($model->project_img_1) ? $model->project_img_1 : '../images/clip-test/pic1.jpg' ?>" alt="" class="clip-image__shape-2"></div>
                </div>
            </div>
            <div class="cell medium-6 large-6">
                <div class="middle-title middle-title--s1"><?= $model->section_2_title ?></div>
                <div class="project-panel__text">
                    <?= Html::encode($model->section_2_text) ?>
                </div>
            </div>
        </div>
    </div>
    <?php if($title = $model->block_1_title) :?>
    <div class="project-box">
        <div class="project-box__title"><?= $title?></div>
        <p class="project-box__text">
            <?= Html::encode($model->block_1_text) ?>
        </p>
        <div class="project-box__image"><img src="<?= ($model->block_1_img) ? $model->block_1_img : '../images/pic-project.png' ?>" alt=""></div>
    </div>
    <?php endif;?>

    <?php if($title = $model->block_2_title) :?>
        <div class="project-box">
            <div class="project-box__title"><?= $title?></div>
            <p class="project-box__text">
                <?= Html::encode($model->block_2_text) ?>
            </p>
            <div class="project-box__image"><img src="<?= ($model->block_2_img) ? $model->block_2_img : '../images/pic-project.png' ?>" alt=""></div>
        </div>
    <?php endif;?>

    <?php if($title = $model->block_3_title) :?>
        <div class="project-box">
            <div class="project-box__title"><?= $title?></div>
            <p class="project-box__text">
                <?= Html::encode($model->block_3_text) ?>
            </p>
            <div class="project-box__image"><img src="<?= ($model->block_3_img) ? $model->block_3_img : '../images/pic-project.png' ?>" alt=""></div>
        </div>
    <?php endif;?>

    <?php if($title = $model->block_4_title) :?>
    <div class="project-box">
        <div class="project-box__title"><?= $title?></div>
        <p class="project-box__text">
            <?= Html::encode($model->block_4_text) ?>
        </p>
        <div class="grid-x grid-padding-x">
            <div class="cell medium-4 large-4">
                <div class="project-box__image"><img src="<?= ($model->block_4_img_1) ? $model->block_4_img_1 : '../images/pic-project1.png' ?>" alt=""></div>
            </div>
            <div class="cell medium-4 large-4">
                <div class="project-box__image"><img src="<?= ($model->block_4_img_2) ? $model->block_4_img_2 : '../images/pic-project1.png' ?>" alt=""></div>
            </div>
            <div class="cell medium-4 large-4">
                <div class="project-box__image"><img src="<?= ($model->block_4_img_3) ? $model->block_4_img_3 : '../images/pic-project1.png' ?>" alt=""></div>
            </div>
        </div>
    </div>
    <?php endif;?>

    <?php if($review_text = $model->review_text) :?>
    <h2 class="main-title main-title--s1">Client’s review</h2>
    <div class="clients-box">
        <div class="clients-box__image"><img src="<?= ($model->review_img) ? $model->review_img : '../images/pic-client.jpg' ?>" alt=""></div>
        <div class="clients-box__title"> <?= $model->review_name ?></div>
        <div class="clients-box__pos"> <?= $model->review_position ?></div>
        <p class="clients-box__text">
            <?= Html::encode($review_text) ?>
        </p>
    </div>
    <?php endif;?>

    <?= \frontend\widgets\ContactWidget::widget() ?>

</div>

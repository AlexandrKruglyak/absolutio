<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;

$index++;
?>
<?php if ($index % 2 == 1): ?>
    <div class="grid-x grid-padding-x align-justify">
        <div class="cell medium-6 large-6">
            <div class="middle-title middle-title--s1"><?= $model->project_name ?></div>
            <div class="project-panel__text">
                <?= Html::encode(StringHelper::truncate($model->project_description,190)) ?>
            </div>
            <div class="project-panel__more">
                <a href="/p-<?= $model->slug ?>" class="button small">Read more</a>
            </div>
        </div>
        <div class="cell medium-5 large-5">
            <div class="project-panel__image">
                <div class="clip-image clip-image--s1">
                    <img
                        src="<?= ($model->project_img_1) ? $model->project_img_1 : '../images/clip-test/pic.jpg' ?>"
                        alt="" class="clip-image__shape-1"
                    >
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="grid-x grid-padding-x align-justify">
        <div class="cell medium-4 large-4 project-panel__col-order">
            <div class="project-panel__image project-panel__image-----p1">
                <div class="clip-image clip-image--s2">
                    <img
                        src="<?= ($model->project_img_1) ? $model->project_img_1 : '../images/clip-test/pic1.jpg' ?>"
                        alt="" class="clip-image__shape-2"
                    >
                </div>
            </div>
        </div>
        <div class="cell medium-6 large-6">
            <div class="shapes">
                <div class="shapes__item-2 shapes__item-2--p1"></div>
            </div>
            <div class="middle-title middle-title--s1"><?= $model->project_name ?></div>
            <div class="project-panel__text">
                <?= Html::encode(StringHelper::truncate($model->project_description,190)) ?>
            </div>
            <div class="project-panel__more">
                <a href="/p-<?= $model->slug ?>" class="button small">Read more</a>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="grid-container page-404">
    <div class="top-logo__404"><a href="/pages/"><img src="../images/logo.svg" alt=""></a></div>
    <div class="grid-x grid-padding-x align-center">
        <div class="cell medium-10 large-8">
            <div class="page-404__box"><img src="../images/title-404.png" alt="" class="page-404__title-img">
                <div class="page-404__title"> <?= nl2br(Html::encode($message)) ?></div>
                <div class="hide-for-small-only page-404__go-home"><a href="/" class="button white">Go Home</a>
                </div>
            </div>
            <div class="show-for-small-only page-404__go-home"><a href="/" class="button white">Go Home</a></div>
        </div>
    </div>
</div>

<svg width="0" height="0">
    <defs>
        <clipPath id="shape1">
            <path d="M56.1,60.6C-48.4,144,13.2,222,78.6,275c65.4,53,161.2,80.7,214.5-22.5C346.3,149.3,345.7,85.8,270.5,38  c-36.2-23-63.1-38-92.9-38C145.6,0,110.2,17.3,56.1,60.6"></path>
        </clipPath>
        <clipPath id="shape2">
            <path d="M182.3,3.6C13.8,36,3.5,118.5,0.5,155.8c-5,63.5,23.9,186,160.8,186h0.1c11,0,22.8-0.8,35.3-2.5  c167.4-22.7,167.4-114.9,167.4-144.3c0-27.2-11.4-195-145.8-195.1C207.2,0,195.2,1.1,182.3,3.6"></path>
        </clipPath>
        <clipPath id="shape3">
            <path d="M71.4,299.6c105.5,54.4,170.3,53.7,219.2-23c48.9-76.8,62.3-112.5-23-219.2c-85.3-106.8-165-43.9-219.2,23  C-5.8,147.3-34.1,245.2,71.4,299.6"></path>
        </clipPath>
    </defs>
</svg>
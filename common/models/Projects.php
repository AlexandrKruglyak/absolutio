<?php

namespace common\models;

use backend\models\ImageModel;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $project_name
 * @property string $project_description
 * @property string $project_img_1
 * @property string $project_img_1_hidden
 * @property string $section_1_title
 * @property string $section_1_text
 * @property string $section_2_title
 * @property string $section_2_text
 * @property string $client_name
 * @property string $website
 * @property string $services
 * @property string $block_1_title
 * @property string $block_1_text
 * @property string $block_1_img
 * @property string $block_1_img_hidden
 * @property string $block_2_title
 * @property string $block_2_text
 * @property string $block_2_img
 * @property string $block_2_img_hidden
 * @property string $block_3_title
 * @property string $block_3_text
 * @property string $block_3_img
 * @property string $block_3_img_hidden
 * @property string $block_4_title
 * @property string $block_4_text
 * @property string $block_4_img_1
 * @property string $block_4_img_1_hidden
 * @property string $block_4_img_2
 * @property string $block_4_img_2_hidden
 * @property string $block_4_img_3
 * @property string $block_4_img_3_hidden
 * @property string $review_name
 * @property string $review_position
 * @property string $review_text
 * @property string $review_img
 * @property string $review_img_hidden
 * @property string $slug
 */
class Projects extends \yii\db\ActiveRecord
{
    public $project_img_1_hidden;
    public $block_1_img_hidden;
    public $block_2_img_hidden;
    public $block_3_img_hidden;
    public $block_4_img_1_hidden;
    public $block_4_img_2_hidden;
    public $block_4_img_3_hidden;
    public $review_img_hidden;

    public function behaviors()
    {
        return [
            'sluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'project_name',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_name'], 'required'],
            [['description', 'project_description', 'section_1_text', 'section_2_text', 'services', 'block_1_text', 'block_2_text', 'block_3_text', 'block_4_text', 'review_text'], 'string'],
            [['title', 'keywords', 'project_name', 'project_img_1', 'section_1_title', 'section_2_title', 'client_name', 'website', 'block_1_title', 'block_1_img', 'block_2_title', 'block_2_img', 'block_3_title', 'block_3_img', 'block_4_title', 'block_4_img_1', 'block_4_img_2', 'block_4_img_3', 'review_name', 'review_position', 'review_img', 'slug',
                'project_img_1_hidden',
                'block_1_img_hidden',
                'block_2_img_hidden',
                'block_3_img_hidden',
                'block_4_img_1_hidden',
                'block_4_img_2_hidden',
                'block_4_img_3_hidden',
                'review_img_hidden',
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'project_name' => 'Project Name',
            'project_description' => 'Project Description',
            'project_img_1' => 'Project Img (500px*500px)',
            'section_1_title' => 'Section 1 Title',
            'section_1_text' => 'Section 1 Text',
            'section_2_title' => 'Section 2 Title',
            'section_2_text' => 'Section 2 Text',
            'client_name' => 'Client Name',
            'website' => 'Website',
            'services' => 'Services',
            'block_1_title' => 'Block 1 Title',
            'block_1_text' => 'Block 1 Text',
            'block_1_img' => 'Block 1 Img (1170px*600px)',
            'block_2_title' => 'Block 2 Title',
            'block_2_text' => 'Block 2 Text',
            'block_2_img' => 'Block 2 Img (1170px*600px)',
            'block_3_title' => 'Block 3 Title',
            'block_3_text' => 'Block 3 Text',
            'block_3_img' => 'Block 3 Img (1170px*600px)',
            'block_4_title' => 'Block 4 Title',
            'block_4_text' => 'Block 4 Text',
            'block_4_img_1' => 'Block 4 Img 1 (320px*620px)',
            'block_4_img_2' => 'Block 4 Img 2 (320px*620px)',
            'block_4_img_3' => 'Block 4 Img 3 (320px*620px)',
            'review_name' => 'Review Name',
            'review_position' => 'Review Position',
            'review_text' => 'Review Text',
            'review_img' => 'Review Img (120px*112px)',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->session->setFlash('success', 'Form successfully saved');
    }

    public function afterDelete()
    {
        Yii::$app->session->setFlash('success', 'Item successfully deleted');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($file = $this->project_img_1_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->project_img_1 = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->project_img_1 = $file;
            }
        }

        if ($file = $this->block_1_img_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_1_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_1_img = $file;
            }
        }

        if ($file = $this->block_2_img_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_2_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_2_img = $file;
            }
        }

        if ($file = $this->block_3_img_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_3_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_3_img = $file;
            }
        }

        if ($file = $this->block_4_img_1_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_4_img_1 = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_4_img_1 = $file;
            }
        }

        if ($file = $this->block_4_img_2_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_4_img_2 = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_4_img_2 = $file;
            }
        }

        if ($file = $this->block_4_img_3_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->block_4_img_3 = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_4_img_3 = $file;
            }
        }

        if ($file = $this->review_img_hidden) {
            if ($path = ImageModel::copy($file,'project')){
                $this->review_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->review_img = $file;
            }
        }

        return true;
    }

    /**
     * Render projects for menu
     * @return string
     */
    public static function renderProjects()
    {
        $projects = self::find()->all();
        $data = '';
        foreach ($projects as $item) {
            $data .= '<li><a href="/p-' . $item->slug . '">' . $item->project_name . '</a></li>';
        }
        return $data;
    }

    public function renderServices()
    {
        $services = explode(',', $this->services);
        $data = '';
        foreach ($services as $service) {
            $data .= '<li>' . $service . '</li>';
        }
        return $data;
    }
}

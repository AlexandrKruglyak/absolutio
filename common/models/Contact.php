<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $contact_title
 * @property string $contact_text
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $contact_address
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $linkedin
 */
class Contact extends \yii\db\ActiveRecord
{
    const ID = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_text'], 'string'],
            ['contact_email','email'],
            [['title', 'keywords','description', 'contact_title', 'contact_phone', 'contact_email', 'contact_address', 'facebook', 'instagram', 'twitter', 'linkedin'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'contact_title' => 'Contact Title',
            'contact_text' => 'Contact Text',
            'contact_phone' => 'Contact Phone',
            'contact_email' => 'Contact Email',
            'contact_address' => 'Contact Address',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'linkedin' => 'Linkedin',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        Yii::$app->session->setFlash('success', 'Form successfully saved');
    }
}

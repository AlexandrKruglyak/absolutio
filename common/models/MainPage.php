<?php

namespace common\models;

use backend\models\ImageModel;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "main_page".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $section_1_title
 * @property string $section_1_text
 * @property string $section_1_link
 * @property string $section_1_img
 * @property string $section_2_title
 * @property string $section_2_text
 * @property string $section_2_img
 * @property string $services_title
 * @property string $services_text
 * @property int $services_1
 * @property int $services_2
 * @property int $services_3
 * @property int $services_4
 * @property int $work_1
 * @property int $work_2
 * @property int $work_3
 * @property string $work_title
 * @property string $work_text
 * @property string $crew_title
 * @property string $crew_text
 * @property string $footer_left_title
 * @property string $footer_left_text
 * @property string $footer_right_title
 * @property string $footer_right_text
 * @property string $section_1_img_hidden
 * @property string $section_2_img_hidden
 */
class MainPage extends \yii\db\ActiveRecord
{
    public $section_1_img_hidden;
    public $section_2_img_hidden;

    const ID = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'section_1_text', 'section_2_text', 'services_text', 'work_text', 'crew_text', 'footer_left_text', 'footer_right_text'], 'string'],
            [['services_1', 'services_2', 'services_3', 'services_4', 'work_1', 'work_2', 'work_3'], 'integer'],
            [['services_1'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_1' => 'id']],
            [['services_2'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_2' => 'id']],
            [['services_3'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_3' => 'id']],
            [['services_4'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_4' => 'id']],
            [['work_1'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['work_1' => 'id']],
            [['work_2'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['work_2' => 'id']],
            [['work_3'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['work_3' => 'id']],
            [['section_1_img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['title', 'keywords', 'section_1_title', 'section_1_link', 'section_2_title', 'section_2_img', 'services_title', 'work_title', 'crew_title', 'footer_left_title', 'footer_right_title',
                'section_1_img_hidden',
                'section_2_img_hidden',
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'section_1_title' => 'Section 1 Title',
            'section_1_text' => 'Section 1 Text',
            'section_1_link' => 'Section 1 Link',
            'section_1_img' => 'Section 1 Img (990px*1200px)',
            'section_2_title' => 'Section 2 Title',
            'section_2_text' => 'Section 2 Text',
            'section_2_img' => 'Section 2 Img (740px*1080px)',
            'services_title' => 'Services Title',
            'services_text' => 'Services Text',
            'services_1' => 'Services 1',
            'services_2' => 'Services 2',
            'services_3' => 'Services 3',
            'services_4' => 'Services 4',
            'work_title' => 'Work Title',
            'work_text' => 'Work Text',
            'work_1' => 'Work 1',
            'work_2' => 'Work 2',
            'work_3' => 'Work 3',
            'crew_title' => 'Crew Title',
            'crew_text' => 'Crew Text',
            'footer_left_title' => 'Footer Left Title',
            'footer_left_text' => 'Footer Left Text',
            'footer_right_title' => 'Footer Right Title',
            'footer_right_text' => 'Footer Right Text',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        Yii::$app->session->setFlash('success', 'Form successfully saved');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($file = $this->section_1_img_hidden) {
            if ($path = ImageModel::copy($file,'main')){
                $this->section_1_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->section_1_img = $file;
            }
        }

        if ($file = $this->section_2_img_hidden) {
            if ($path = ImageModel::copy($file,'main')){
                $this->section_2_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->section_2_img = $file;
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices1()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices2()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices3()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_3']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices4()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_4']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork1()
    {
        return $this->hasOne(Projects::className(), ['id' => 'work_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork2()
    {
        return $this->hasOne(Projects::className(), ['id' => 'work_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork3()
    {
        return $this->hasOne(Projects::className(), ['id' => 'work_3']);
    }
}

<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Projects;

/**
 * ProjectsSearch represents the model behind the search form of `common\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'keywords', 'project_name', 'project_description', 'project_img_1', 'section_1_title', 'section_1_text', 'section_2_title', 'section_2_text', 'client_name', 'website', 'services', 'block_1_title', 'block_1_text', 'block_1_img', 'block_2_title', 'block_2_text', 'block_2_img', 'block_3_title', 'block_3_text', 'block_3_img', 'block_4_title', 'block_4_text', 'block_4_img_1', 'block_4_img_2', 'block_4_img_3', 'review_name', 'review_position', 'review_text', 'review_img'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'project_name', $this->project_name])
            ->andFilterWhere(['like', 'project_description', $this->project_description])
            ->andFilterWhere(['like', 'project_img_1', $this->project_img_1])
            ->andFilterWhere(['like', 'section_1_title', $this->section_1_title])
            ->andFilterWhere(['like', 'section_1_text', $this->section_1_text])
            ->andFilterWhere(['like', 'section_2_title', $this->section_2_title])
            ->andFilterWhere(['like', 'section_2_text', $this->section_2_text])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'services', $this->services])
            ->andFilterWhere(['like', 'block_1_title', $this->block_1_title])
            ->andFilterWhere(['like', 'block_1_text', $this->block_1_text])
            ->andFilterWhere(['like', 'block_1_img', $this->block_1_img])
            ->andFilterWhere(['like', 'block_2_title', $this->block_2_title])
            ->andFilterWhere(['like', 'block_2_text', $this->block_2_text])
            ->andFilterWhere(['like', 'block_2_img', $this->block_2_img])
            ->andFilterWhere(['like', 'block_3_title', $this->block_3_title])
            ->andFilterWhere(['like', 'block_3_text', $this->block_3_text])
            ->andFilterWhere(['like', 'block_3_img', $this->block_3_img])
            ->andFilterWhere(['like', 'block_4_title', $this->block_4_title])
            ->andFilterWhere(['like', 'block_4_text', $this->block_4_text])
            ->andFilterWhere(['like', 'block_4_img_1', $this->block_4_img_1])
            ->andFilterWhere(['like', 'block_4_img_2', $this->block_4_img_2])
            ->andFilterWhere(['like', 'block_4_img_3', $this->block_4_img_3])
            ->andFilterWhere(['like', 'review_name', $this->review_name])
            ->andFilterWhere(['like', 'review_position', $this->review_position])
            ->andFilterWhere(['like', 'review_text', $this->review_text])
            ->andFilterWhere(['like', 'review_img', $this->review_img]);

        return $dataProvider;
    }
}

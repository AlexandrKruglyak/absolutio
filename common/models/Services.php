<?php

namespace common\models;

use backend\models\ImageModel;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $service_name
 * @property string $service_description
 * @property string $block_1_title
 * @property string $block_1_text
 * @property string $block_1_img
 * @property string $block_2_title
 * @property string $block_2_text
 * @property string $block_2_img
 * @property string $block_3_title
 * @property string $block_3_text
 * @property string $block_3_img
 * @property string $block_4_title
 * @property string $block_4_text
 * @property string $block_4_img
 * @property string $block_5_title
 * @property string $block_5_text
 * @property string $block_5_img
 * @property string $slug
 * @property string $block_1_img_hidden
 * @property string $block_2_img_hidden
 * @property string $block_3_img_hidden
 * @property string $block_4_img_hidden
 * @property string $block_5_img_hidden
 */
class Services extends \yii\db\ActiveRecord
{
    public $block_1_img_hidden;
    public $block_2_img_hidden;
    public $block_3_img_hidden;
    public $block_4_img_hidden;
    public $block_5_img_hidden;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'service_name',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_name'], 'required'],
            [['description', 'service_description', 'block_1_text', 'block_2_text', 'block_3_text', 'block_4_text', 'block_5_text'], 'string'],
            [['title', 'keywords', 'service_name', 'block_1_title', 'block_1_img', 'block_2_title', 'block_2_img', 'block_3_title', 'block_3_img', 'block_4_title', 'block_4_img', 'block_5_title', 'block_5_img',
                'slug',
                'block_1_img_hidden',
                'block_2_img_hidden',
                'block_3_img_hidden',
                'block_4_img_hidden',
                'block_5_img_hidden',
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'service_name' => 'Service Name',
            'service_description' => 'Service Description',
            'block_1_title' => 'Block 1 Title',
            'block_1_text' => 'Block 1 Text',
            'block_1_img' => 'Block 1 Img (500px*500px)',
            'block_2_title' => 'Block 2 Title',
            'block_2_text' => 'Block 2 Text',
            'block_2_img' => 'Block 2 Img (500px*500px)',
            'block_3_title' => 'Block 3 Title',
            'block_3_text' => 'Block 3 Text',
            'block_3_img' => 'Block 3 Img (500px*500px)',
            'block_4_title' => 'Block 4 Title',
            'block_4_text' => 'Block 4 Text',
            'block_4_img' => 'Block 4 Img (500px*500px)',
            'block_5_title' => 'Block 5 Title',
            'block_5_text' => 'Block 5 Text',
            'block_5_img' => 'Block 5 Img (500px*500px)',
            'slug' => 'Slug',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        Yii::$app->session->setFlash('success', 'Form successfully saved');
    }

    public function afterDelete()
    {
        Yii::$app->session->setFlash('success', 'Item successfully deleted');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($file = $this->block_1_img_hidden) {
            if ($path = ImageModel::copy($file,'service')){
                $this->block_1_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_1_img = $file;
            }
        }

        if ($file = $this->block_2_img_hidden) {
            if ($path = ImageModel::copy($file,'service')){
                $this->block_2_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_2_img = $file;
            }
        }

        if ($file = $this->block_3_img_hidden) {
            if ($path = ImageModel::copy($file,'service')){
                $this->block_3_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_3_img = $file;
            }
        }

        if ($file = $this->block_4_img_hidden) {
            if ($path = ImageModel::copy($file,'service')){
                $this->block_4_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_4_img = $file;
            }
        }

        if ($file = $this->block_5_img_hidden) {
            if ($path = ImageModel::copy($file,'service')){
                $this->block_5_img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->block_5_img = $file;
            }
        }

        return true;
    }
}

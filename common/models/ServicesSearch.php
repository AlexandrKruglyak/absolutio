<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Services;

/**
 * ServicesSearch represents the model behind the search form of `common\models\Services`.
 */
class ServicesSearch extends Services
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'keywords', 'service_name', 'service_description', 'block_1_title', 'block_1_text', 'block_1_img', 'block_2_title', 'block_2_text', 'block_2_img', 'block_3_title', 'block_3_text', 'block_3_img', 'block_4_title', 'block_4_text', 'block_4_img', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Services::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'service_name', $this->service_name])
            ->andFilterWhere(['like', 'service_description', $this->service_description])
            ->andFilterWhere(['like', 'block_1_title', $this->block_1_title])
            ->andFilterWhere(['like', 'block_1_text', $this->block_1_text])
            ->andFilterWhere(['like', 'block_1_img', $this->block_1_img])
            ->andFilterWhere(['like', 'block_2_title', $this->block_2_title])
            ->andFilterWhere(['like', 'block_2_text', $this->block_2_text])
            ->andFilterWhere(['like', 'block_2_img', $this->block_2_img])
            ->andFilterWhere(['like', 'block_3_title', $this->block_3_title])
            ->andFilterWhere(['like', 'block_3_text', $this->block_3_text])
            ->andFilterWhere(['like', 'block_3_img', $this->block_3_img])
            ->andFilterWhere(['like', 'block_4_title', $this->block_4_title])
            ->andFilterWhere(['like', 'block_4_text', $this->block_4_text])
            ->andFilterWhere(['like', 'block_4_img', $this->block_4_img])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}

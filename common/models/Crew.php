<?php

namespace common\models;

use backend\models\ImageModel;
use Yii;

/**
 * This is the model class for table "crew".
 *
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $img
 * @property string $img_hidden
 */
class Crew extends \yii\db\ActiveRecord
{
    public $img_hidden;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crew';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'img', 'img_hidden'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
            'img' => 'Img (304px*285px)',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        Yii::$app->session->setFlash('success', 'Form successfully saved');
    }

    public function afterDelete()
    {
        Yii::$app->session->setFlash('success', 'Item successfully deleted');
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($file = $this->img_hidden) {
            if ($path = ImageModel::copy($file,'crew')){
                $this->img = $path;
                ImageModel::removeFile($file,'tmp');
            }else{
                $this->img = $file;
            }
        }
        return true;
    }
}

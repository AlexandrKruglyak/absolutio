<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');


function dump($target, $exit = true, $depth = 10, $highlight = true)
{
    echo \yii\helpers\VarDumper::dumpAsString($target, $depth, $highlight);
    if ($exit) {
        exit();
    }
}

function getModelName($className) :string
{
    return array_reverse(explode('\\', $className))[0];
}

function getUserIP()
{
    $ipaddress = '';
    if(yii::$app->request->getUserIP()){return yii::$app->request->getUserIP();}

    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = '127.0.0.1';

    return $ipaddress;

}

function getDateTime($time = false, $format ='Y-m-d H:i:s' ){
    $time = $time ?: time();
    return date($format, $time);
}

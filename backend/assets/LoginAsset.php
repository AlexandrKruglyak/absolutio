<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{


    public $basePath = '@webroot';
    public $baseUrl = '@web/';

    public $css = [
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all',
        'css/font-awesome.min.css',
        'css/font-awesome/css/font-awesome.min.css',
        'css/simple-line-icons/simple-line-icons.min.css',
        'css/bootstrap.css',
        'css/bootstrap-switch.css',
        'css/select2.min.css',
        'css/select2-bootstrap.min.css',
        'css/components.min.css',
        'css/plugins.min.css',
        'css/login-4.min.css',
    ];
    public $js = [
//        'js/respond.min.js',
//        'js/excanvas.min.js',
//        'js/ie8.fix.min.js',

        'js/jquery.min.js',
        'js/bootstrap.min.js',
        'js/js.cookie.min.js',
        'js/jquery.slimscroll.min.js',
        'js/jquery.blockui.min.js',
        'js/bootstrap-switch.min.js',
        'js/jquery.validate.min.js',
        'js/additional-methods.min.js',
        'js/select2.full.min.js',
        'js/jquery.backstretch.min.js',
        'js/app.min.js',
        'js/login-4.min.js',
];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\widgets\ActiveFormAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'common\assets\AdminLte',
//        'common\assets\Html5shiv'
    ];
}

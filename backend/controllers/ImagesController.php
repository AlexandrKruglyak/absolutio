<?php

namespace backend\controllers;

use backend\models\ImageModel;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class ImagesController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['ajax-upload', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $field
     * @param $model_name
     * @return bool|string
     */
    public function actionAjaxUpload($field, $model_name)
    {
        $return = ImageModel::ajax($field, $model_name);
        return $return;
    }

    public function actionDelete()
    {
        $return = ImageModel::del(Yii::$app->request->post('key'));
        return $return;
    }
}
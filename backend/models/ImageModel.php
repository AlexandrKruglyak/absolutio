<?php

namespace backend\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ImageModel extends \yii\base\Model
{

    const TEMP_FOLDER = '/assets/images/tmp/';
    const IMG_FOLDER = '/assets/images/';

    /**
     * @param $file
     * @param $folder
     * @return string
     */
    public static function copy($file, $folder)
    {
        $target_folder = Yii::getAlias('@front') . self::IMG_FOLDER . $folder;
        if (!is_dir($target_folder))
            FileHelper::createDirectory($target_folder, 0775, true);

        $tmp_file = getcwd() . self::TEMP_FOLDER . $file;
        $target_file = $target_folder . '/' . $file;

        if (!is_file($tmp_file))
            return false;

        if (copy($tmp_file, $target_file))
            return self::IMG_FOLDER . $folder . '/' . $file;
    }

    /**
     * @param $file
     * @param $folder
     */
    public static function removeFile($file, $folder)
    {
        unlink(getcwd() . self::IMG_FOLDER . $folder . '/' . $file);
    }

    /**
     * @param $field
     * @param $model_name
     * @return bool|string
     */
    public static function ajax($field, $model_name)
    {
        $short_model_name = explode('\\',$model_name);
        if ($file = $_FILES[$short_model_name[2]]) {
            $model = new $model_name();
            $model->$field = $file;
            $save_field = mb_strtolower($short_model_name[2]).'-'.$field.'_hidden';
            $path = getcwd() . self::TEMP_FOLDER;

            if (!is_dir($path))
                FileHelper::createDirectory($path, 0775, true);

            if (isset($model)) {
                $time = time();
                $file = UploadedFile::getInstance($model, $field);
                $file->saveAs($path . '/' . md5($file->baseName . $time) . '.' . $file->extension);
                $data = [
                    'file' => md5($file->baseName . $time) . '.' . $file->extension,
                    'field' => $save_field
                ];
                return json_encode($data);
            }
        }

        return false;
    }

    /**
     * @param $key
     * @return string
     */
    public static function del($key)
    {
        $data = explode(':', $key);

        $model = $data[2]::findOne($data[0]);
        $field = $data[1];
        $file = $model->$field;
        $model->$field = '';
        if ($model->save() && is_file(Yii::getAlias('@front') . $file)){
            unlink(Yii::getAlias('@front') . $file);
        }

        return json_encode($key);
    }

    /**
     * @param $model
     * @param $field
     * @param $count
     * @param $minImageWidth
     * @param $minImageHeight
     * @return array
     */
    public static function config($model,$field,$count,$minImageWidth,$minImageHeight)
    {
        $class = get_class($model);
        $preview = [];
        $previewConfig = [];
        if ($model->$field) {
            $preview = [[str_replace('admin.', '', \yii\helpers\Url::home(true)) . $model->$field]];
            $previewConfig = [['caption' => 'Image', 'url' => \yii\helpers\Url::to('/images/delete'), 'key' => $model->id . ':'.$field.':'.$class]];
        }
        return [
            'options' => [
                'accept' => 'image/*',
                'id' => 'photos'.$count,
            ],
            'pluginOptions' => [
                //'showPreview' => true,
                'uploadUrl' => '/images/ajax-upload?field='.$field.'&model_name='.$class,
                'maxFileSize' => 10000,
                'maxFileCount' => 1,
                'minImageWidth' => $minImageWidth,
                'minImageHeight' => $minImageHeight,
                'validateInitialCount' => true,
                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                'uploadAsync' => false,
                'overwriteInitial' => false,
                'initialPreview' => $preview,
                'initialPreviewAsData' => true,
                'browseOnZoneClick' => true,
                'showUpload' => false,
                'showBrowse' => false,
                'fileActionSettings' => ['showZoom' => false, 'showRemove' => true, 'showUpload' => false],
                'initialPreviewConfig' => $previewConfig
            ]
        ];
    }
}
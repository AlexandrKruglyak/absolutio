<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Crew */

$this->title = 'Update Crew: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Crews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h1 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>

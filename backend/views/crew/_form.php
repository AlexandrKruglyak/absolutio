<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Crew */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crew-form">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'm-form'],
        'fieldConfig' => ['options' => ['class' => 'form-group m-form__group']],
    ]); ?>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->img_hidden = $model->img; ?>
                            <?= $form->field($model, 'img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'img',1,270,280)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

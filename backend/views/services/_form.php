<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'm-form'],
    'fieldConfig' => ['options' => ['class' => 'form-group m-form__group']],
]); ?>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Service</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <?= $form->field($model, 'service_name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'service_description')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Block 1</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_1_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_1_text')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?php $model->block_1_img_hidden = $model->block_1_img; ?>
                        <?= $form->field($model, 'block_1_img_hidden')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'block_1_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model, 'block_1_img', 1, 500, 500)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Block 2</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_2_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_2_text')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?php $model->block_2_img_hidden = $model->block_2_img; ?>
                        <?= $form->field($model, 'block_2_img_hidden')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'block_2_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model, 'block_2_img', 2, 500, 500)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Block 3</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_3_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_3_text')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?php $model->block_3_img_hidden = $model->block_3_img; ?>
                        <?= $form->field($model, 'block_3_img_hidden')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'block_3_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model, 'block_3_img', 3, 500, 500)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Block 4</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_4_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_4_text')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?php $model->block_4_img_hidden = $model->block_4_img; ?>
                        <?= $form->field($model, 'block_4_img_hidden')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'block_4_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model, 'block_4_img', 4, 500, 500)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-form__heading">
    <span class="m-form__heading-title">Fields for Block 5</span>
</div>
<div class="m-section__content">
    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
        <div class="m-demo__preview">
            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_5_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= $form->field($model, 'block_5_text')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?php $model->block_5_img_hidden = $model->block_5_img; ?>
                        <?= $form->field($model, 'block_5_img_hidden')->hiddenInput()->label(false) ?>
                        <?= $form->field($model, 'block_5_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model, 'block_5_img', 5, 500, 500)) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>


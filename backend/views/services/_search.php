<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ServicesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="services-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'keywords') ?>

    <?= $form->field($model, 'service_name') ?>

    <?php // echo $form->field($model, 'service_description') ?>

    <?php // echo $form->field($model, 'block_1_title') ?>

    <?php // echo $form->field($model, 'block_1_text') ?>

    <?php // echo $form->field($model, 'block_1_img') ?>

    <?php // echo $form->field($model, 'block_2_title') ?>

    <?php // echo $form->field($model, 'block_2_text') ?>

    <?php // echo $form->field($model, 'block_2_img') ?>

    <?php // echo $form->field($model, 'block_3_title') ?>

    <?php // echo $form->field($model, 'block_3_text') ?>

    <?php // echo $form->field($model, 'block_3_img') ?>

    <?php // echo $form->field($model, 'block_4_title') ?>

    <?php // echo $form->field($model, 'block_4_text') ?>

    <?php // echo $form->field($model, 'block_4_img') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Services */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</p>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h1 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'description:ntext',
                'keywords',
                'service_name',
                'service_description:ntext',
                'block_1_title',
                'block_1_text:ntext',
                'block_1_img',
                'block_2_title',
                'block_2_text:ntext',
                'block_2_img',
                'block_3_title',
                'block_3_text:ntext',
                'block_3_img',
                'block_4_title',
                'block_4_text:ntext',
                'block_4_img',
                'slug',
            ],
        ]) ?>
    </div>
</div>

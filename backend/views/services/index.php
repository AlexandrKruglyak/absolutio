<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a('Create Services', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'service_name',
            'service_description:ntext',
            //'block_1_title',
            //'block_1_text:ntext',
            //'block_1_img',
            //'block_2_title',
            //'block_2_text:ntext',
            //'block_2_img',
            //'block_3_title',
            //'block_3_text:ntext',
            //'block_3_img',
            //'block_4_title',
            //'block_4_text:ntext',
            //'block_4_img',
            //'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div>
</div>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="m-section">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'm-form'],
        'fieldConfig' => ['options' => ['class' => 'form-group m-form__group']],
    ]); ?>

    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Contact info</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'contact_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'contact_text')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'contact_address')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="m-form__heading">
        <span class="m-form__heading-title">Social</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col">
                            <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col">
                            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>
</div>

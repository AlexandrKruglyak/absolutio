<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainPage */

$this->title = 'Main Page';
?>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>

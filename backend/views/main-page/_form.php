<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MainPage */
/* @var $form yii\widgets\ActiveForm */
$service_array = \yii\helpers\ArrayHelper::map(\common\models\Services::find()->all(), 'id', 'service_name');
$work_array = \yii\helpers\ArrayHelper::map(\common\models\Projects::find()->all(), 'id', 'project_name');
?>

<div class="m-section">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'm-form'],
        'fieldConfig' => ['options' => ['class' => 'form-group m-form__group']],
    ]); ?>

    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Section 1</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'section_1_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'section_1_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'section_1_link')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php
                            $model->section_1_img_hidden = $model->section_1_img;
                            ?>
                            <?= $form->field($model, 'section_1_img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'section_1_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'section_1_img',1,990,1200)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Section 2</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'section_2_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'section_2_text')->textarea(['rows' => 6]) ?>
                    <?php $model->section_2_img_hidden = $model->section_2_img; ?>
                    <?= $form->field($model, 'section_2_img_hidden')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'section_2_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'section_2_img',2,740,1080)) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Services section</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">

                    <?= $form->field($model, 'services_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'services_text')->textarea(['rows' => 6]) ?>

                    <div class="row">
                        <div class="col">
                            <?= $form->field($model, 'services_1')->dropDownList($service_array, ['prompt' => 'Select service 1']) ?>
                            <?= $form->field($model, 'services_3')->dropDownList($service_array, ['prompt' => 'Select service 3']) ?>
                        </div>
                        <div class="col">
                            <?= $form->field($model, 'services_2')->dropDownList($service_array, ['prompt' => 'Select service 2']) ?>
                            <?= $form->field($model, 'services_4')->dropDownList($service_array, ['prompt' => 'Select service 4']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Works section</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'work_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'work_text')->textarea(['rows' => 6]) ?>

                    <div class="row">
                        <div class="col">
                            <?= $form->field($model, 'work_1')->dropDownList($work_array, ['prompt' => 'Select work 1']) ?>
                        </div>
                        <div class="col">
                            <?= $form->field($model, 'work_2')->dropDownList($work_array, ['prompt' => 'Select work 2']) ?>
                        </div>
                        <div class="col">
                            <?= $form->field($model, 'work_3')->dropDownList($work_array, ['prompt' => 'Select work 3']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Crew section</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'crew_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'crew_text')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Footer</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col">
                            <?= $form->field($model, 'footer_left_title')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'footer_left_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col">
                            <?= $form->field($model, 'footer_right_title')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'footer_right_text')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>
</div>

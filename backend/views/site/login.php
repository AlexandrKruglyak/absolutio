<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="/img/logo1.png" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'login-form']); ?>

    <h3 class="form-title">Login to your account</h3>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span> Enter any username and password. </span>
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <?php echo $form->field($model, 'username')->textInput(['class'=>'form-control', 'style'=>'padding-left:30px', 'placeholder'=>'Username'])->label(false) ?>
            <!--                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>-->
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <?php echo $form->field($model, 'password')->passwordInput(['class'=>'form-control', 'style'=>'padding-left:30px', 'placeholder'=>'Password'])->label(false) ?>
                <!--                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>-->
            </div>
            <div class="form-actions">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <?php echo $form->field($model, 'rememberMe')->checkbox(['class'=>'simple']) ?>
                    <!--                <input type="checkbox" name="remember" value="1" /> Remember me-->
                    <!--                <span></span>-->
                </label>
                <?php echo \yii\helpers\Html::submitButton('Login', [
                    'class' => 'btn green pull-right',
                    'name' => 'login-button'
                ]) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
            <!-- END REGISTRATION FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> <?php echo date('Y' )?> &copy; Absolutio - Admin Panel. </div>
        <!-- END COPYRIGHT -->

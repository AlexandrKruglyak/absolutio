<?php
use backend\assets\BackendAsset;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */

$bundle = \backend\assets\AppAsset::register($this);

$this->params['body-class'] = array_key_exists('body-class', $this->params) ? $this->params['body-class'] : null;
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>" class="wf-poppins-n3-active wf-poppins-n4-active wf-roboto-n5-active wf-roboto-n6-active wf-roboto-n7-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-roboto-n3-active wf-roboto-n4-active wf-active">
        <head>
            <meta charset="<?php echo Yii::$app->charset ?>">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

            <!--begin::Web font -->
            <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
            <script>
                WebFont.load({
                    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                    active: function() {
                        sessionStorage.fonts = true;
                    }
                });
            </script>
            <!--end::Web font -->
            <!-- favicon -->
            <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192" href="/img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
            <link rel="manifest" href="/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <?php echo Html::csrfMetaTags() ?>
            <title>Admin-panel</title>
            <?php $this->head() ?>
        </head>
        <?php $this->beginBody() ?>
        <?php echo $content ?>
        <?php $this->endBody() ?>
        <?php echo Html::tag('body', '', ['class'=>'m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default']) ?>
        <?php Html::endTag('body') ?>
    </html>
<?php $this->endPage() ?>

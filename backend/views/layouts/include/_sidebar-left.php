<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
            class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item--submenu <?php echo preg_match('/page-/', yii::$app->request->url) ? 'm-menu__item--open' : '' ?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle"><i
                            class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">Content</span><i
                            class="m-menu__ver-arrow la la-angle-right"></i></a>
                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item <?php echo preg_match('/main-page\//', yii::$app->request->url) ? 'm-menu__item--active' : '' ?>"
                            aria-haspopup="true"><a href="/main-page/" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Main page</span></a>
                        </li>
                        <li class="m-menu__item <?php echo preg_match('/contact\//', yii::$app->request->url) ? 'm-menu__item--active' : '' ?>"
                            aria-haspopup="true"><a href="/contact/" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Contact</span></a>
                        </li>
                        <li class="m-menu__item <?php echo preg_match('/work\//', yii::$app->request->url) ? 'm-menu__item--active' : '' ?>"
                            aria-haspopup="true"><a href="/work/" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Work</span></a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--<?php echo yii::$app->request->url == '/projects' ? 'active' : '' ?> ">
                <a href="/projects" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-squares-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Projects</span>
                    </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item m-menu__item--<?php echo yii::$app->request->url == '/services' ? 'active' : '' ?> ">
                <a href="/services" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-interface-3"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Services</span>
                    </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item m-menu__item--<?php echo yii::$app->request->url == '/crew' ? 'active' : '' ?> ">
                <a href="/crew" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-avatar"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Crew</span>
                    </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item m-menu__item--<?php echo yii::$app->request->url == '/notification' ? 'active' : '' ?> ">
                <a href="/notification" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-signs-2"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Notification</span>
                    </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item m-menu__item--<?php echo yii::$app->request->url == '/users' ? 'active' : '' ?> ">
                <a href="/users" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Users</span>
                    </span></span>
                </a>
            </li>
        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
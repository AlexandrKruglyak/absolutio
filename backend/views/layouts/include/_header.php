<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
    <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand  m-brand--skin-dark ">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="/" class="m-brand__logo-wrapper">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 544.47 124.64">
                              <path d="M209.28 28.5l-5.86 15.6h11.85l-6-15.6zm3.82-11.71l16.42 41.18-7.6 3.59-4.01-10.54h-17l-3.94 10.54h-8.3c-.14 0-.2-.14-.15-.3l17.28-44.34c.04-.09.1-.13.2-.13h7.1zM254.7 41.94h-9.8V54.5h9.22c2.28 0 4.11-.52 5.5-1.58a5.3 5.3 0 0 0 2.07-4.48c0-2.1-.64-3.72-1.91-4.83-1.27-1.12-2.96-1.68-5.07-1.68m-2.83-18.04h-6.98v11.12h6.32c2.37 0 4.22-.45 5.56-1.35 1.34-.9 2.01-2.31 2.01-4.24 0-2.11-.61-3.56-1.84-4.35-1.23-.79-2.92-1.18-5.07-1.18m1.25-7.11c4.34 0 7.88.95 10.6 2.86 2.72 1.9 4.08 5.13 4.08 9.65 0 1.66-.38 3.17-1.15 4.5s-1.77 2.5-3 3.46a14 14 0 0 1 4.97 4.28c1.3 1.8 1.95 4.08 1.95 6.85 0 4.13-1.44 7.35-4.31 9.68-2.88 2.32-6.71 3.49-11.5 3.49h-18.05c-.24 0-.31-.1-.31-.3V16.95c0-.1.03-.16.13-.16h16.59zM277.36 50.84c1.36.7 4.93 2.25 7.04 2.95 2.1.7 4.1 1.05 6 1.05 2.67 0 4.68-.41 6.02-1.25 1.34-.83 2-2.04 2-3.62 0-1.5-.88-2.82-2.66-3.98a63.8 63.8 0 0 0-5.9-3.4 61.82 61.82 0 0 1-9.05-5.69c-2.87-2.17-4.3-5.23-4.3-9.18 0-3.38 1.33-6.23 4-8.53 2.68-2.3 6.48-3.46 11.4-3.46 2.24 0 4.47.35 6.72 1.06 2.23.7 4.2 1.47 5.92 2.3l-3.73 6.43c-1.36-.66-1.93-.87-3.51-1.46-1.58-.6-3.34-.89-5.27-.89-1.89 0-3.48.38-4.77 1.12a3.7 3.7 0 0 0-1.95 3.43c0 1.62.78 2.97 2.34 4.04a46.65 46.65 0 0 0 5.63 3.26l.46.27a61.7 61.7 0 0 1 9.12 5.76 10.76 10.76 0 0 1 4.38 9.05c0 3.56-1.44 6.5-4.31 8.82-2.88 2.33-7.02 3.5-12.42 3.5-1.88 0-4.3-.42-7.27-1.26-2.79-.78-4.71-1.59-5.77-2.41a.59.59 0 0 1-.19-.42l.07-7.49M322.72 39.3c0 4.7.94 8.54 2.83 11.53 1.89 2.98 4.83 4.47 8.82 4.47 3.6 0 6.48-1.34 8.63-4.01 2.15-2.68 3.23-6.8 3.23-12.38 0-4.65-.95-8.47-2.84-11.46-1.88-2.98-4.82-4.48-8.82-4.48-3.51 0-6.36 1.47-8.56 4.42-2.2 2.94-3.29 6.9-3.29 11.91m-8.89.07c0-6.9 1.8-12.52 5.4-16.89 3.6-4.37 8.67-6.62 15.21-6.75 7.02 0 12.22 2.26 15.6 6.78 3.38 4.52 5.07 9.99 5.07 16.4 0 6.94-1.8 12.6-5.43 16.99-3.62 4.39-8.7 6.58-15.24 6.58-6.98 0-12.16-2.25-15.54-6.75-3.38-4.5-5.07-9.95-5.07-16.36M393.96 61.56H364.7c-.22 0-.3-.14-.3-.28V17c0-.14.06-.21.18-.21h8.31V54.9h18.5l2.57 6.65zM408.38 16.79v27.85c0 3.73.9 6.44 2.7 8.13 1.8 1.69 4.13 2.53 6.98 2.53 2.9 0 5.24-.87 7.01-2.63 1.78-1.76 2.67-4.43 2.67-8.03V16.78h8.62V45.3c0 4.83-1.65 8.87-4.97 12.12-3.31 3.25-7.8 4.87-13.46 4.87-5.58 0-10-1.62-13.27-4.87a16.39 16.39 0 0 1-4.9-12.12V17s-.01-.21.2-.21h8.42zM442.23 16.79h34.67l2.65 6.71h-15.34v38.06h-8.14a.5.5 0 0 1-.48-.5V23.5h-13.63V17.1c0-.2.1-.3.27-.3M493.9 16.79v44.77h-8.06c-.29 0-.42-.2-.42-.42V16.98c0-.13.06-.2.16-.2h8.33zM512.08 39.3c0 4.7.94 8.54 2.83 11.53 1.89 2.98 4.83 4.47 8.82 4.47 3.6 0 6.48-1.34 8.63-4.01s3.22-6.8 3.22-12.38c0-4.65-.94-8.47-2.83-11.46-1.89-2.98-4.83-4.48-8.82-4.48-3.51 0-6.37 1.47-8.56 4.42-2.2 2.94-3.3 6.9-3.3 11.91m-8.88.07c0-6.9 1.8-12.52 5.4-16.89 3.6-4.37 8.67-6.62 15.2-6.75 7.03 0 12.23 2.26 15.61 6.78 3.38 4.52 5.07 9.99 5.07 16.4 0 6.94-1.8 12.6-5.43 16.99-3.62 4.39-8.7 6.58-15.24 6.58-6.98 0-12.16-2.25-15.54-6.75-3.38-4.5-5.07-9.95-5.07-16.36" fill="#434958"/>
                              <path d="M208.13 99.26h-1.2l-4.6-16.14a59.08 59.08 0 0 1-.85-3.26c-.15.83-.37 1.78-.65 2.86-.28 1.08-1.82 6.6-4.62 16.54h-1.23l-5.75-20.92h1.53l3.66 13.48.4 1.51a63.6 63.6 0 0 1 .61 2.48c.08.37.16.75.22 1.13.23-1.3.72-3.26 1.46-5.9l3.58-12.7h1.62l4.2 14.57c.48 1.68.83 3.03 1.04 4.06.12-.68.28-1.41.48-2.18.2-.78 1.66-6.26 4.41-16.45h1.47l-5.78 20.92zM232.56 99.26h-11.44V78.34h11.44v1.35h-9.98v7.91H232v1.35h-9.42v8.97h9.98zM263.49 93.87c0 1.74-.64 3.13-1.93 4.15-1.28 1.02-2.98 1.53-5.11 1.53-2.56 0-4.52-.28-5.88-.84v-1.46c1.5.64 3.43.96 5.77.96 1.71 0 3.07-.4 4.08-1.19 1-.78 1.51-1.81 1.51-3.1 0-.79-.17-1.44-.5-1.96-.33-.52-.88-1-1.63-1.43-.75-.42-1.86-.89-3.32-1.38-2.14-.74-3.61-1.53-4.43-2.39-.82-.85-1.22-1.99-1.22-3.41 0-1.56.6-2.84 1.83-3.83a7.3 7.3 0 0 1 4.73-1.48c1.97 0 3.82.37 5.54 1.12l-.53 1.26a12.9 12.9 0 0 0-4.98-1.1c-1.54 0-2.77.37-3.69 1.08a3.5 3.5 0 0 0-1.37 2.92c0 .77.14 1.4.42 1.9.28.5.74.95 1.38 1.34.64.4 1.74.87 3.3 1.43 1.62.56 2.85 1.1 3.67 1.64a5.25 5.25 0 0 1 1.8 1.8c.37.67.56 1.48.56 2.44M271.9 88.77c0 3 .67 5.32 2 6.95 1.34 1.64 3.26 2.46 5.76 2.46 2.51 0 4.44-.82 5.77-2.44 1.33-1.62 2-3.94 2-6.97 0-3.01-.67-5.32-2-6.93-1.33-1.6-3.24-2.41-5.74-2.41s-4.41.81-5.76 2.43c-1.35 1.62-2.03 3.93-2.03 6.91m17.12 0c0 3.32-.83 5.95-2.5 7.88-1.65 1.93-3.94 2.9-6.86 2.9-2.91 0-5.2-.97-6.86-2.9-1.66-1.93-2.49-4.57-2.49-7.9s.84-5.95 2.51-7.87c1.68-1.91 3.97-2.87 6.87-2.87 2.92 0 5.2.96 6.85 2.89 1.65 1.92 2.48 4.54 2.48 7.87M297.34 99.26V78.34h1.46V97.9h9.98v1.37zM328.26 78.34h1.6l-7.83 20.92h-1.3l-7.8-20.92h1.57l5.26 14.24c.8 2.15 1.34 3.76 1.62 4.84.2-.72.57-1.83 1.13-3.34l5.75-15.74zM347.77 99.26h-11.44V78.34h11.44v1.35h-9.98v7.91h9.42v1.35h-9.42v8.97h9.98zM367.15 78.34h1.46v20.92h-1.46zM383.5 99.26h-1.47V79.72h-6.8v-1.38h15.07v1.38h-6.8z" fill="#3d6fb6"/>
                              <linearGradient id="main-logo-grad1" gradientUnits="userSpaceOnUse" x1="123.86" y1="-.71" x2="146.66" y2="128.64" gradientTransform="matrix(1 0 0 -1 0 126)">
                                <stop offset="0" stop-color="#1484c6"/>
                                <stop offset=".16" stop-color="#2493cc"/>
                                <stop offset=".49" stop-color="#40b0d8"/>
                                <stop offset=".79" stop-color="#52c1df"/>
                                <stop offset="1" stop-color="#58c7e1"/>
                              </linearGradient>
                              <path d="M112.11 13.49v111.15h18.79c30.79-1.38 30.79-35.4 30.79-35.4V0L112.1 13.49z" fill="url(#main-logo-grad1)"/>
                              <linearGradient id="main-logo-grad2" gradientUnits="userSpaceOnUse" x1="-6.67" y1="12.91" x2="168.58" y2="114.09" gradientTransform="matrix(1 0 0 -1 0 126)">
                                <stop offset="0" stop-color="#88cccf"/>
                                <stop offset=".46" stop-color="#2ebcee"/>
                                <stop offset="1" stop-color="#485ca0"/>
                              </linearGradient>
                              <path d="M128.29 0c-1.37 0-27.77.32-47.46 24.6C60.9 49.19 1.64 122.6 0 124.63h34.03c5.2-.3 28.46-2.6 42.76-20.17L161.7 0h-33.36-.05" fill="url(#main-logo-grad2)"/>
                            </svg>
                        </a>
                    </div>
                    <div class="m-stack__item m-stack__item--middle m-brand__tools">

                        <!-- BEGIN: Left Aside Minimize Toggle -->
                        <a href="javascript:;" id="m_aside_left_minimize_toggle"
                           class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                            <span></span>
                        </a>

                        <!-- END -->

                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle"
                           class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>

                        <!-- END -->

                        <!-- BEGIN: Responsive Header Menu Toggler -->
                        <!--                                        <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">-->
                        <!--                                            <span></span>-->
                        <!--                                        </a>-->

                        <!-- END -->

                        <!-- BEGIN: Topbar Toggler -->
                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
                           class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                            <i class="flaticon-more"></i>
                        </a>

                        <!-- BEGIN: Topbar Toggler -->
                    </div>
                </div>
            </div>

            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                <!-- BEGIN: Horizontal Menu -->
                <!-- END: Horizontal Menu -->

                <!-- BEGIN: Topbar -->
                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                    <div class="m-stack__item m-topbar__nav-wrapper">
                        <ul class="m-topbar__nav m-nav m-nav--inline">
                            <!--                            <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">-->
                            <!--                                <p class="m-nav__link m-dropdown__toggle">-->
                            <!--                                    <span class="m-nav__link-text">-->
                            <!--                                        <i style="font-size: 25px;" class="m-menu__link-icon flaticon-earth-globe"></i>-->
                            <!--                                    </span>-->
                            <!--                                </p>-->
                            <!--                            </li>-->
                            <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                m-dropdown-toggle="click">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="/img/anonymous.jpg" class="m--img-rounded m--marginless"
                                                         alt=""/>
                                                </span>
                                    <span style="font-weight: bold; color: #0c0c0c !important; padding-left: 10px"
                                          class="m-topbar__username m--show"><?php echo(yii::$app->user->identity->username) ?></span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center"
                                             style="background: url(/assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                            <div class="m-card-user m-card-user--skin-dark">
                                                <div class="m-card-user__pic">
                                                    <img src="<?php ?>" class="m--img-rounded m--marginless" alt=""/>
                                                </div>
                                                <div class="m-card-user__details">
                                                    <span class="m-card-user__name m--font-weight-500"><?php ?></span>
                                                    <a href=""
                                                       class="m-card-user__email m--font-weight-300 m-link"><?php echo(yii::$app->user->identity->email) ?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav m-nav--skin-light">
                                                    <li class="m-nav__section m--hide">
                                                        <span class="m-nav__section-text">Section</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="/users/update?id=<?= Yii::$app->user->id ?>"
                                                           class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                            <span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">My Profile</span>
																			</span>
																		</span>
                                                        </a>
                                                    </li>

                                                    <li class="m-nav__item">
                                                        <a href="/users/change-password"
                                                           class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lock"></i>
                                                            <span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">Change Password</span>
																			</span>
																		</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <?= \yii\helpers\Html::a('Logout', \yii\helpers\Url::to(['site/logout']), ['data-method' => 'post', 'class' => 'btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder']) ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- END: Topbar -->
            </div>
        </div>
    </div>
</header>

<!-- END: Header -->

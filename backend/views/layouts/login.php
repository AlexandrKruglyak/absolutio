<?php
use backend\assets\BackendAsset;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */

$bundle = \backend\assets\LoginAsset::register($this);

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
<!--    <meta name="msapplication-config" content="img/favicon/browserconfig.xml">-->
    <meta name="theme-color" content="#ffffff">
    <?php echo Html::csrfMetaTags() ?>
    <title>Admin-panel</title>

    <?php $this->head() ?>

</head>
<?php $this->beginBody() ?>
<?php echo $content ?>
<?php $this->endBody() ?>
<?php echo Html::tag('body', '', ['class'=>'login']) ?>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<?php Html::endTag('body') ?>
</html>
<?php $this->endPage() ?>

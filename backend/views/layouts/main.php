<?php
use yii\helpers\ArrayHelper;
use common\components\sweetalert2\Sweetalert2;
/**
 * @var $this yii\web\View
 */
?>

<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- begin::Header -->
        <?php echo $this->render('include/_header') ?>
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <?php echo $this->render('include/_sidebar-left') ?>
            <!-- begin::Content -->

            <div class="m-grid__item m-grid__item--fluid m-wrapper m-content">
<!--                <div class="alert alert-success" role="alert">-->
<!--                    <strong>Well done!</strong> You successfully read this important alert message.-->
<!--                </div>-->
                <?= \backend\widgets\Alert::widget()?>
                <?php echo $content ?>
            </div>
        </div>
        <!-- begin::Footer -->
        <?php echo $this->render('include/_footer') ?>

        <!-- begin::Quick sidebar -->
<!--        --><?php //echo $this->render('include/_quick-sidebar') ?>

        <!-- begin::Scroll Top -->
        <?php echo $this->render('include/_scroll') ?>

        <!-- begin::Quick Nav -->
<!--        --><?php //echo $this->render('include/_quick-nav') ?>
    </div>
<?php $this->endContent(); ?>
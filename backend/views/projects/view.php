<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</p>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h1 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'description:ntext',
                'keywords',
                'project_name',
                'project_description:ntext',
                'project_img_1',
                'section_1_title',
                'section_1_text:ntext',
                'section_2_title',
                'section_2_text:ntext',
                'client_name',
                'website',
                'services:ntext',
                'block_1_title',
                'block_1_text:ntext',
                'block_1_img',
                'block_2_title',
                'block_2_text:ntext',
                'block_2_img',
                'block_3_title',
                'block_3_text:ntext',
                'block_3_img',
                'block_4_title',
                'block_4_text:ntext',
                'block_4_img_1',
                'block_4_img_2',
                'block_4_img_3',
                'review_name',
                'review_position',
                'review_text:ntext',
                'review_img',
            ],
        ]) ?>
    </div>
</div>


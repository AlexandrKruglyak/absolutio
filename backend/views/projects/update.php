<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = 'Update Projects: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h1 class="m-portlet__head-text">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>

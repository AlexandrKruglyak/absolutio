<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'keywords') ?>

    <?= $form->field($model, 'project_name') ?>

    <?php // echo $form->field($model, 'project_description') ?>

    <?php // echo $form->field($model, 'project_img') ?>

    <?php // echo $form->field($model, 'section_1_title') ?>

    <?php // echo $form->field($model, 'section_1_text') ?>

    <?php // echo $form->field($model, 'section_2_title') ?>

    <?php // echo $form->field($model, 'section_2_text') ?>

    <?php // echo $form->field($model, 'client_name') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'services') ?>

    <?php // echo $form->field($model, 'block_1_title') ?>

    <?php // echo $form->field($model, 'block_1_text') ?>

    <?php // echo $form->field($model, 'block_1_img') ?>

    <?php // echo $form->field($model, 'block_2_title') ?>

    <?php // echo $form->field($model, 'block_2_text') ?>

    <?php // echo $form->field($model, 'block_2_img') ?>

    <?php // echo $form->field($model, 'block_3_title') ?>

    <?php // echo $form->field($model, 'block_3_text') ?>

    <?php // echo $form->field($model, 'block_3_img') ?>

    <?php // echo $form->field($model, 'block_4_title') ?>

    <?php // echo $form->field($model, 'block_4_text') ?>

    <?php // echo $form->field($model, 'block_4_img_1') ?>

    <?php // echo $form->field($model, 'block_4_img_2') ?>

    <?php // echo $form->field($model, 'block_4_img_3') ?>

    <?php // echo $form->field($model, 'review_name') ?>

    <?php // echo $form->field($model, 'review_position') ?>

    <?php // echo $form->field($model, 'review_text') ?>

    <?php // echo $form->field($model, 'review_img') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

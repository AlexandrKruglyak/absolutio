<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="m-section">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'm-form'],
        'fieldConfig' => ['options' => ['class' => 'form-group m-form__group']],
    ]); ?>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Project</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'project_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'project_description')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->project_img_1_hidden = $model->project_img_1; ?>
                            <?= $form->field($model, 'project_img_1_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'project_img_1')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'project_img_1',1,500,500)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Section 1</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'section_1_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'section_1_text')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Section 2</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'section_2_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'section_2_text')->textarea(['rows' => 6]) ?>

                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Client</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'services',['template' => '{label}{input} <span class="m-form__help">Separate items with a comma.</span>{error}'])->textInput(['maxlength' => true])  ?>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Block 1</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">

                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_1_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_1_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->block_1_img_hidden = $model->block_1_img; ?>
                            <?= $form->field($model, 'block_1_img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_1_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_1_img',2,1170,600)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Block 2</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_2_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_2_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->block_2_img_hidden = $model->block_2_img; ?>
                            <?= $form->field($model, 'block_2_img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_2_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_2_img',3,1170,600)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Block 3</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_3_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_3_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->block_3_img_hidden = $model->block_3_img; ?>
                            <?= $form->field($model, 'block_3_img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_3_img')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_3_img',4,1170,600)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Block 4</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_4_title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'block_4_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-4">
                            <?php $model->block_4_img_1_hidden = $model->block_4_img_1; ?>
                            <?= $form->field($model, 'block_4_img_1_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_4_img_1')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_4_img_1',5,320,620)) ?>
                        </div>
                        <div class="col-xs-4">
                            <?php $model->block_4_img_2_hidden = $model->block_4_img_2; ?>
                            <?= $form->field($model, 'block_4_img_2_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_4_img_2')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_4_img_2',6,320,620)) ?>
                        </div>
                        <div class="col-xs-4">
                            <?php $model->block_4_img_3_hidden = $model->block_4_img_3; ?>
                            <?= $form->field($model, 'block_4_img_3_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'block_4_img_3')->widget(FileInput::classname(), \backend\models\ImageModel::config($model,'block_4_img_3',7,320,620)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__heading">
        <span class="m-form__heading-title">Fields for Review</span>
    </div>
    <div class="m-section__content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview">
                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'review_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'review_position')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'review_text')->textarea(['rows' => 6]) ?>
                        </div>
                        <div class="col-xs-12">
                            <?php $model->review_img_hidden = $model->review_img; ?>
                            <?= $form->field($model, 'review_img_hidden')->hiddenInput()->label(false) ?>
                            <?= $form->field($model, 'review_img')->widget(FileInput::classname(),\backend\models\ImageModel::config($model,'review_img',8,120,112)) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

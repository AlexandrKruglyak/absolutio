$(document).ready(function () {

    $("#photos1").on("filebatchselected", function (event, files) {
      $("#photos1").fileinput("upload");
    });

    $('#photos1').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $('#photos1').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $("#photos2").on("filebatchselected", function (event, files) {
        $("#photos2").fileinput("upload");
    });

    $('#photos2').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
        // console.log(response.field);
        // alert (extra.bdInteli + " " +  response.uploaded);
    });

    $('#photos2').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos3").on("filebatchselected", function (event, files) {
        $("#photos3").fileinput("upload");
    });

    $('#photos3').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos3').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos4").on("filebatchselected", function (event, files) {
        $("#photos4").fileinput("upload");
    });

    $('#photos4').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos4').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos5").on("filebatchselected", function (event, files) {
        $("#photos5").fileinput("upload");
    });

    $('#photos5').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos5').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos6").on("filebatchselected", function (event, files) {
        $("#photos6").fileinput("upload");
    });

    $('#photos6').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos6').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos7").on("filebatchselected", function (event, files) {
        $("#photos7").fileinput("upload");
    });

    $('#photos7').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos7').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });

    $("#photos8").on("filebatchselected", function (event, files) {
        $("#photos8").fileinput("upload");
    });

    $('#photos8').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        $('#'+response.field).val(response.file);
    });

    $('#photos8').on('filepredelete', function(event, key, jqXHR, data) {
        var field = key.split(':')
        var model_name = field[2].split('\\');
        $('#'+model_name[2].toLowerCase()+'-'+field[1]+'_hidden').val('');
    });
})